<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Shop Homepage</title>

  <!-- Bootstrap core CSS -->
  <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="/var/assets/css/shop-homepage.css" rel="stylesheet">

</head>
<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">My Apparel Shops</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="one/">Shop One</a>
          </li>
          <li class="nav-item active">
            |
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="two/">Shop Two</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">

    <div class="row"  style="margin-top: 40px !important;">

      <div class="col-lg-3">

        <h1 class="my-4" style="margin-top: 4.5rem!important">One-Stop Shop</h1>
        <h3 class="my-4" style="margin-top: 4.5rem!important">Shop here!</h3>
        <div class="list-group">
          <?= $this->category; ?>
        </div>

      </div>
      <!-- /.col-lg-3 -->

      <div class="col-lg-9"  style="margin-top: 40px !important;">

        <div id="ProductTable" class="row">
          <?= $this->productTable; ?>
        </div>
        <!-- /.row -->

      </div>
      <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>
  <!-- <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


  <script >
  $('.list-group-item').click(function(){
      console.log('here');
      var itemName =  $(this).html().toLowerCase();

      //Check if parent category
      if ($('#' + itemName + '-child').length > 0) {
        console.log('!undefined');
          //Open drawer
          $('#' + itemName + '-child > a').each(function() {
            var subItemName = $(this).html();
            if ($('#' +  subItemName).hasClass('open')) {
              $('#' +  subItemName).css("display", "none")
              $('#' +  subItemName).removeClass('open');
            } else {
              $('#' +  subItemName).css("display", "block")
              $('#' +  subItemName).addClass('open');
            }
          });
      } else { //No child element, Load Products
          console.log('itemName:' + itemName);
          if ($(this).attr('id') == 'all') {
              location.reload();
          } else {
            $.ajax({
                url:window.location.origin+'/categorize',
                async: true,
                data: {'category' : itemName},
                type:'POST',
                success: function(data){
                    console.log(data);
                    if (data != '') {
                       $('#ProductTable').html(data);
                    } else {
                       $('#ProductTable').html('No Products Found');
                    }
                }
            });
          }
      }

  });
    
  </script>
</body>

</html>
