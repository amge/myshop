<?php
/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

/**
* @Route("/api/")
*/
namespace AppBundle\Controller;

use Pimcore\Http\RequestHelper;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\DataObject\Products as Products;
use Pimcore\Model\DataObject\Category  as Category;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ApiController extends FrontendController
{

    /**
	* @Route("categorize")
    * @Method("POST")
	*/
    public function categorizeAction(Request $request)
    {
        $category = $request->get('category');
        $productTable = '';
        
        //All product listing
        $products = new Products\Listing();
        $products->setCondition('o_published =  ?', 1);
        // $products->setCondition("Category LIKE '%,object|".$category.",%'");
        $products->load();

        foreach ($products as $product) {
        	$productCategories = $product->getCategory();
        	foreach ($productCategories as $productCategory) {
        		$thisCategory = strtolower($productCategory->getCategoryName());
        		if ($thisCategory == $category) {
        			$productTable .= '
		                <div class="col-lg-4 col-md-6 mb-4">
		                    <div class="card h-100">
		                      <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
		                      <div class="card-body">
		                        <h4 class="card-title">
		                          <a href="#">' . $product->getName() .'</a>
		                        </h4>
		                        <h5>$'.$product->getPrice().'</h5>
		                        <p class="card-text">'.$product->getDescription().'</p>
		                      </div>
		                    </div>
		                </div>';
        		}
        	}
        } 
      	echo $productTable;
  		die;
    }

}
