<?php
/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\Controller;

use Pimcore\Http\RequestHelper;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\DataObject\Products as Products;
use Pimcore\Model\DataObject\Category  as Category;
use Symfony\Component\HttpFoundation\Request;

class ShopController extends AbstractController
{

    /**
     * show product list
     */
    public function listOneAction(Request $request)
    {
        //Get website folder id
        $websiteOneConfig = \Pimcore\Model\WebsiteSetting::getByName('WebsiteOneFolderId');
        $websiteOneId = $websiteOneConfig->getData();

        //All product listing
        $products = new Products\Listing();
        $products->setCondition('o_published =  ?', 1);
        $products->load();
        $productTable = '';

        foreach ($products as $product) {
            $productTable .= '
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="card h-100">
                      <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                      <div class="card-body">
                        <h4 class="card-title">
                          <a href="#">' . $product->getName() .'</a>
                        </h4>
                        <h5>$'.$product->getPrice().'</h5>
                        <p class="card-text">'.$product->getDescription().'</p>
                      </div>
                    </div>
                </div>';
        }

        $this->view->productTable = $productTable;

        //Category
        $category = new Category\Listing();
        $category->setCondition('o_published =  ? AND o_parentId = ?', [1, $websiteOneId]);
        $category->load();

        $categoryTable .= '<a id="all" href="#" class="list-group-item">All Products</a>';
        foreach ($category as $categoryItem) {
            //Get Object by path
            $categoryParent = AbstractObject::getByPath($categoryItem);
            $categoryName = $categoryParent->getCategoryName();

            $categoryTable .= '<a href="#" class="list-group-item">'. $categoryName .'</a>';

            //Get children
            $categoryChildren = $categoryParent->getCategoryChild();
            $iChild = count($categoryChildren);
            $loop = 0;
            if ($iChild) {
                $categoryTable .= '<div id="'. strtolower($categoryName) .'-child">';
                do {
                    $categoryTable .= '<a id="'. $categoryChildren[$loop]->getCategoryName() .'" href="#" class="list-group-item" style="display:none;">'. $categoryChildren[$loop]->getCategoryName() .'</a>';
                    $loop++;
                } while ($loop < $iChild);
                $categoryTable .= '</div>';
            }
        }
        $this->view->category = $categoryTable;
    }

    public function listTwoAction(Request $request)
    {
        //Get website folder id
        $websiteTwoConfig = \Pimcore\Model\WebsiteSetting::getByName('WebsiteTwoFolderId');
        $websiteTwoId = $websiteTwoConfig->getData();

        //All product listing
        $products = new Products\Listing();
        $products->setCondition('o_published =  ?', 1);
        $products->load();
        $productTable = '';

        foreach ($products as $product) {
            $productTable .= '
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="card h-100">
                      <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                      <div class="card-body">
                        <h4 class="card-title">
                          <a href="#">' . $product->getName() .'</a>
                        </h4>
                        <h5>$'.$product->getPrice().'</h5>
                        <p class="card-text">'.$product->getDescription().'</p>
                      </div>
                    </div>
                </div>';
        }

        $this->view->productTable = $productTable;

        //Category
        $category = new Category\Listing();
        $category->setCondition('o_published =  ? AND o_parentId = ?', [1, $websiteTwoId]);
        $category->load();

        $categoryTable .= '<a id="all" href="#" class="list-group-item">All Products</a>';
        foreach ($category as $categoryItem) {
            //Get Object by path
            $categoryParent = AbstractObject::getByPath($categoryItem);
            $categoryName = $categoryParent->getCategoryName();

            $categoryTable .= '<a href="#" class="list-group-item">'. $categoryName .'</a>';

            //Get children
            $categoryChildren = $categoryParent->getCategoryChild();
            $iChild = count($categoryChildren);
            $loop = 0;
            if ($iChild) {
                $categoryTable .= '<div id="'. strtolower($categoryName) .'-child">';
                do {
                    $categoryTable .= '<a id="'. $categoryChildren[$loop]->getCategoryName() .'" href="#" class="list-group-item" style="display:none;">'. $categoryChildren[$loop]->getCategoryName() .'</a>';
                    $loop++;
                } while ($loop < $iChild);
                $categoryTable .= '</div>';
            }
        }
        $this->view->category = $categoryTable;
    }

}
