<?php 

/** 
* Generated at: 2019-06-18T12:58:12+02:00
* Inheritance: no
* Variants: no
* Changed by: admin (2)
* IP: 192.168.34.1


Fields Summary: 
- CategoryId [input]
- CategoryName [input]
- CategoryChild [manyToManyObjectRelation]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\Category\Listing getByCategoryId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Category\Listing getByCategoryName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Category\Listing getByCategoryChild ($value, $limit = 0) 
*/

class Category extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "2";
protected $o_className = "Category";
protected $CategoryId;
protected $CategoryName;
protected $CategoryChild;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Category
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get CategoryId - CategoryId
* @return string
*/
public function getCategoryId () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("CategoryId"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->CategoryId;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set CategoryId - CategoryId
* @param string $CategoryId
* @return \Pimcore\Model\DataObject\Category
*/
public function setCategoryId ($CategoryId) {
	$fd = $this->getClass()->getFieldDefinition("CategoryId");
	$this->CategoryId = $CategoryId;
	return $this;
}

/**
* Get CategoryName - CategoryName
* @return string
*/
public function getCategoryName () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("CategoryName"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->CategoryName;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set CategoryName - CategoryName
* @param string $CategoryName
* @return \Pimcore\Model\DataObject\Category
*/
public function setCategoryName ($CategoryName) {
	$fd = $this->getClass()->getFieldDefinition("CategoryName");
	$this->CategoryName = $CategoryName;
	return $this;
}

/**
* Get CategoryChild - CategoryChild
* @return \Pimcore\Model\DataObject\Category[]
*/
public function getCategoryChild () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("CategoryChild"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("CategoryChild")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set CategoryChild - CategoryChild
* @param \Pimcore\Model\DataObject\Category[] $CategoryChild
* @return \Pimcore\Model\DataObject\Category
*/
public function setCategoryChild ($CategoryChild) {
	$fd = $this->getClass()->getFieldDefinition("CategoryChild");
	$currentData = $this->getCategoryChild();
	$isEqual = $fd->isEqual($currentData, $CategoryChild);
	if (!$isEqual) {
		$this->markFieldDirty("CategoryChild", true);
	}
	$this->CategoryChild = $fd->preSetData($this, $CategoryChild);
	return $this;
}

}

