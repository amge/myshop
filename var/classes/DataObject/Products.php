<?php 

/** 
* Generated at: 2019-06-18T16:58:52+02:00
* Inheritance: no
* Variants: no
* Changed by: admin (2)
* IP: 192.168.34.1


Fields Summary: 
- Sku [input]
- Name [input]
- Description [input]
- Sizes [select]
- Colors [select]
- Image1 [image]
- Image2 [image]
- Image3 [image]
- Category [manyToManyObjectRelation]
- Price [numeric]
- Gender [select]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\Products\Listing getBySku ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Products\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Products\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Products\Listing getBySizes ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Products\Listing getByColors ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Products\Listing getByImage1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Products\Listing getByImage2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Products\Listing getByImage3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Products\Listing getByCategory ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Products\Listing getByPrice ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Products\Listing getByGender ($value, $limit = 0) 
*/

class Products extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "1";
protected $o_className = "Products";
protected $Sku;
protected $Name;
protected $Description;
protected $Sizes;
protected $Colors;
protected $Image1;
protected $Image2;
protected $Image3;
protected $Category;
protected $Price;
protected $Gender;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Products
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get Sku - Sku
* @return string
*/
public function getSku () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Sku"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Sku;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Sku - Sku
* @param string $Sku
* @return \Pimcore\Model\DataObject\Products
*/
public function setSku ($Sku) {
	$fd = $this->getClass()->getFieldDefinition("Sku");
	$this->Sku = $Sku;
	return $this;
}

/**
* Get Name - Name
* @return string
*/
public function getName () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Name"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Name;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Name - Name
* @param string $Name
* @return \Pimcore\Model\DataObject\Products
*/
public function setName ($Name) {
	$fd = $this->getClass()->getFieldDefinition("Name");
	$this->Name = $Name;
	return $this;
}

/**
* Get Description - Description
* @return string
*/
public function getDescription () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Description"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Description;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Description - Description
* @param string $Description
* @return \Pimcore\Model\DataObject\Products
*/
public function setDescription ($Description) {
	$fd = $this->getClass()->getFieldDefinition("Description");
	$this->Description = $Description;
	return $this;
}

/**
* Get Sizes - Sizes
* @return string
*/
public function getSizes () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Sizes"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Sizes;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Sizes - Sizes
* @param string $Sizes
* @return \Pimcore\Model\DataObject\Products
*/
public function setSizes ($Sizes) {
	$fd = $this->getClass()->getFieldDefinition("Sizes");
	$this->Sizes = $Sizes;
	return $this;
}

/**
* Get Colors - Colors
* @return string
*/
public function getColors () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Colors"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Colors;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Colors - Colors
* @param string $Colors
* @return \Pimcore\Model\DataObject\Products
*/
public function setColors ($Colors) {
	$fd = $this->getClass()->getFieldDefinition("Colors");
	$this->Colors = $Colors;
	return $this;
}

/**
* Get Image1 - Image1
* @return \Pimcore\Model\Asset\Image
*/
public function getImage1 () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Image1"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Image1;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Image1 - Image1
* @param \Pimcore\Model\Asset\Image $Image1
* @return \Pimcore\Model\DataObject\Products
*/
public function setImage1 ($Image1) {
	$fd = $this->getClass()->getFieldDefinition("Image1");
	$this->Image1 = $Image1;
	return $this;
}

/**
* Get Image2 - Image2
* @return \Pimcore\Model\Asset\Image
*/
public function getImage2 () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Image2"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Image2;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Image2 - Image2
* @param \Pimcore\Model\Asset\Image $Image2
* @return \Pimcore\Model\DataObject\Products
*/
public function setImage2 ($Image2) {
	$fd = $this->getClass()->getFieldDefinition("Image2");
	$this->Image2 = $Image2;
	return $this;
}

/**
* Get Image3 - Image3
* @return \Pimcore\Model\Asset\Image
*/
public function getImage3 () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Image3"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Image3;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Image3 - Image3
* @param \Pimcore\Model\Asset\Image $Image3
* @return \Pimcore\Model\DataObject\Products
*/
public function setImage3 ($Image3) {
	$fd = $this->getClass()->getFieldDefinition("Image3");
	$this->Image3 = $Image3;
	return $this;
}

/**
* Get Category - Category
* @return \Pimcore\Model\DataObject\Category[]
*/
public function getCategory () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Category"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("Category")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Category - Category
* @param \Pimcore\Model\DataObject\Category[] $Category
* @return \Pimcore\Model\DataObject\Products
*/
public function setCategory ($Category) {
	$fd = $this->getClass()->getFieldDefinition("Category");
	$currentData = $this->getCategory();
	$isEqual = $fd->isEqual($currentData, $Category);
	if (!$isEqual) {
		$this->markFieldDirty("Category", true);
	}
	$this->Category = $fd->preSetData($this, $Category);
	return $this;
}

/**
* Get Price - Price
* @return float
*/
public function getPrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Price"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Price;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Price - Price
* @param float $Price
* @return \Pimcore\Model\DataObject\Products
*/
public function setPrice ($Price) {
	$fd = $this->getClass()->getFieldDefinition("Price");
	$this->Price = $fd->preSetData($this, $Price);
	return $this;
}

/**
* Get Gender - Gender
* @return string
*/
public function getGender () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Gender"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Gender;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Gender - Gender
* @param string $Gender
* @return \Pimcore\Model\DataObject\Products
*/
public function setGender ($Gender) {
	$fd = $this->getClass()->getFieldDefinition("Gender");
	$this->Gender = $Gender;
	return $this;
}

}

