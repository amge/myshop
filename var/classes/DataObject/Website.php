<?php 

/** 
* Generated at: 2019-06-18T08:53:16+02:00
* Inheritance: yes
* Variants: no
* Changed by: admin (2)
* IP: 192.168.34.1


Fields Summary: 
- Name [input]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\Website\Listing getByName ($value, $limit = 0) 
*/

class Website extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "3";
protected $o_className = "Website";
protected $Name;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Website
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get Name - Name
* @return string
*/
public function getName () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Name"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Name;

	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("Name")->isEmpty($data)) {
		try {
			return $this->getValueFromParent("Name");
		} catch (InheritanceParentNotFoundException $e) {
			// no data from parent available, continue ... 
		}
	}

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Name - Name
* @param string $Name
* @return \Pimcore\Model\DataObject\Website
*/
public function setName ($Name) {
	$fd = $this->getClass()->getFieldDefinition("Name");
	$this->Name = $Name;
	return $this;
}

}

