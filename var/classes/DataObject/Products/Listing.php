<?php 

namespace Pimcore\Model\DataObject\Products;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Products current()
 * @method DataObject\Products[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "1";
protected $className = "Products";


}
