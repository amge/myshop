<?php 

namespace Pimcore\Model\DataObject\Category;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Category current()
 * @method DataObject\Category[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "2";
protected $className = "Category";


}
