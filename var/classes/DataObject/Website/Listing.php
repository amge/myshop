<?php 

namespace Pimcore\Model\DataObject\Website;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Website current()
 * @method DataObject\Website[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "3";
protected $className = "Website";


}
